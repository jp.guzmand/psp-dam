package bucle;

public class Lanzador {

	public static void main(String[] args) {
		Long tiempoInicial = System.currentTimeMillis();
		
		SumaBestia suma = new SumaBestia();
//		suma.sumar();
//		suma.sumar();
//		suma.sumar();
//		suma.sumar();
		
		
		Thread hilo1 = new Thread(suma);
		Thread hilo2 = new Thread(suma);
		Thread hilo3 = new Thread(suma);
		Thread hilo4 = new Thread(suma);
		hilo1.start();
		hilo2.start();
		hilo3.start();
		hilo4.start();

		Long tiempoFinal = System.currentTimeMillis();
		
		System.out.println("Tiempo tardado: " + (tiempoFinal-tiempoInicial)/1000 + " segundos");
		
	}

}
