package correoTest;

public class Buzon {

	private String mensaje;
	
	public synchronized void escribir(String mensaje) {
		if (this.mensaje != null) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.mensaje = mensaje;
	}
	
	public synchronized String leer() {
		String lectura = mensaje;
		mensaje = null;
		notify();
		return lectura;
	}
}
