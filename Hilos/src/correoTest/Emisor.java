package correoTest;

public class Emisor implements Runnable{

	
	private Buzon buzon;
	
	public Emisor(Buzon buzon) {
		this.buzon = buzon;
	}
	
	
	@Override
	public void run() {
		for (int i = 0; true; i++) {
			buzon.escribir("Mensaje " + i);
		}
	}

}
