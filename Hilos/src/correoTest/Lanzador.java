package correoTest;

public class Lanzador {

	public static void main(String[] args) {
		Buzon buzon = new Buzon();
		Emisor e1 = new Emisor(buzon);
		Receptor r1 = new Receptor(buzon);
		
		Thread hiloEmisor1 = new Thread(e1);
		Thread hiloReceptor1 = new Thread(r1);
		Thread hiloReceptor2 = new Thread(r1);
		Thread hiloReceptor3 = new Thread(r1);
		Thread hiloReceptor4 = new Thread(r1);
		
		hiloEmisor1.start();
		hiloReceptor1.start();
		hiloReceptor2.start();
		hiloReceptor3.start();
		hiloReceptor4.start();
	}
}
