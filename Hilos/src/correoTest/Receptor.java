package correoTest;

public class Receptor implements Runnable {

	private Buzon buzon;
	
	public Receptor(Buzon buzon) {
		this.buzon = buzon;
	}
	
	@Override
	public void run() {
		while(true) {
			String lectura = buzon.leer();
			if (lectura != null) {
				System.out.println("Mensaje recibido: " + lectura);
			}
			else {
				System.out.println("Buz�n vac�o. ");
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
