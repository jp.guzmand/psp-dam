package hilos;

public class Impresora {

	private String usuarioActual = null;
	private Integer numCopias = 0;
	
	public synchronized void imprimir(String usuario, String texto) {
		try {
			if (usuarioActual !=null && !usuarioActual.equals(usuario)) {
				wait();
			}
			if (usuarioActual == null) {
				usuarioActual = usuario;
			}
		
			System.out.println(usuario + " - " + texto);
			Thread.sleep(10);
		
			numCopias++;
			if (numCopias >= 5) {
				numCopias = 0;
				usuarioActual = null;
				notify();
				wait();
			}
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}
}
