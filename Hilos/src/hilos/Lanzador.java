package hilos;

public class Lanzador {

	public static void main(String[] args) {
		Impresora impresora = new Impresora(); // �nica y compartida

		
		Escritor escritor1 = new Escritor("Blas", impresora);
		Escritor escritor2 = new Escritor("Epi", impresora);
		Escritor escritor3 = new Escritor("Laura", impresora);
		
		Thread hilo1 = new Thread(escritor1);
		hilo1.start();
		
		Thread hilo2 = new Thread(escritor2);
		hilo2.start();
		
		Thread hilo3 = new Thread(escritor3);
		hilo3.start();

	}
}
