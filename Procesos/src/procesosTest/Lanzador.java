package procesosTest;

import java.io.File;

public class Lanzador {

	public static void main(String[] args) {
		try {

			ProcessBuilder pb = new ProcessBuilder("java", "procesosTest.ProcesoSuma");
			File directorioClases = new File("C:\\vistas\\ceu\\procesos\\psp-dam\\Procesos\\bin");
			pb.directory(directorioClases);
			
			Long tiempoInicial = System.currentTimeMillis();
			
			Process procesoBestia1 = pb.start();
			
			procesoBestia1.waitFor();

			Long tiempoFinal = System.currentTimeMillis();
			
			
			System.out.println("He terminado y he tardado = "+  (tiempoFinal-tiempoInicial)/1000 + " segundos");
		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
